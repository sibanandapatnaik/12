import { Component, OnInit } from '@angular/core';
import { advertiseCrowsel, Advertise ,advertiseOne } from "app/models/advertise";

@Component({
  selector: 'app-adv-side',
  templateUrl: 'advertise-side-component.html',
  styleUrls: ['advertise-side-component.css','../../../assets/css/home-page.css','../../../assets/css/responsive.css'
   ,'../../login/login.component.css',
   '../../../assets/css/header.css']
})
export class AdvertiseSideComponent implements  OnInit{
     advertiseCrowsel : Advertise[];
    advertiseOne : Advertise;
     ngOnInit() {
     this.advertiseCrowsel = advertiseCrowsel;
     this.advertiseOne = advertiseOne;
   }

}