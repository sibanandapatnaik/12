import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { AboutusComponent } from './aboutus.component';
import { AboutusRoutes } from './aboutus.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import { EditorComponent } from "app/editor/editor.component";
////import { DashboardModule } from '../dashboard/dashboard.module';

@NgModule({
    imports: [CommonModule,  RouterModule.forChild(AboutusRoutes)],
    declarations: [AboutusComponent, Footer , Topnav,EditorComponent],
    exports: [AboutusComponent,EditorComponent]
})

export class AboutusModule { }
