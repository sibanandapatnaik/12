import { Routes } from '@angular/router';
import { AboutusComponent } from './index';
import { EditorComponent } from "app/editor/editor.component";
//import { DashboardRoutes} from '../dashboard/index';
export const AboutusRoutes: Routes = [
    {
      path: 'aboutus', 
      component: AboutusComponent     
    },
     {
      path: 'editor', 
      component: EditorComponent     
    }
];
