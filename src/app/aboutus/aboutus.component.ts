import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-aboutus-cmp',
  templateUrl: 'aboutus.component.html',
   styleUrls: ['../login/login.component.css','../../assets/css/header.css','../../assets/css/home-page.css','../../assets/css/responsive.css']
})

export class AboutusComponent { }
