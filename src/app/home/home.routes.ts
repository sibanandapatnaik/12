import { Routes } from '@angular/router';
import { HomeComponent } from './index';
import { LoginComponent } from '../login/index';
import { LoginRoutes } from '../login/login.routes';
////import { DashboardRoutes } from '../dashboard/dashboard.routes';

export const HomeRoutes: Routes = [
    { path: '',
      component: HomeComponent
    }
];
