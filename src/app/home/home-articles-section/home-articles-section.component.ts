import { Component, OnInit  } from '@angular/core';
import { Article ,articlesList} from "app/models/article";
import { Router } from "@angular/router";
@Component({
  selector: 'home-articles-section',
  templateUrl: 'home-articles-section.component.html',
   styleUrls: ['../../../assets/css/header.css']
})

export class HomeArticlesSectionComponent implements  OnInit {
   articleList:Article[] = new Array();
   article :Article;
  constructor(private router: Router){
    
  }
  ngOnInit(): void {
    //this.newsList = newsList.copyWithin(0,3);
   for (let i = 0; i < 6; i++) {
      this.article = articlesList[i];
        this.articleList.push(this.article) ;
      
    }
    
     console.log("Length of header Size: "+this.articleList.length)
  }
  
  onArticleDetail(newsSelect : Article){
      console.log("method is called"+newsSelect.title)
      this.router.navigate(['/articles-detail',newsSelect.uniqueId]);
}
}

