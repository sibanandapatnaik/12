import {Component, OnInit, Input, OnChanges} from '@angular/core';
import {Image} from '../image';
import { advertiseHomeScreen, Advertise  } from "app/models/advertise";

const IMAGES: Image[] = [
  {'name': 'First', 'url': 'assets/img/home-add-screen.jpg'},
  {'name': 'Second', 'url': 'assets/img/home-add-screen2.jpg'},
  {'name': 'Third', 'url': 'assets/img/home-add-screen3.jpg'},
  {'name': 'Fourth', 'url': 'assets/img/home-add-screen4.jpg'}
];

@Component({
  selector: 'home-carousel',
  templateUrl: 'home-carousel.component.html',
  styleUrls: ['home-carousel.component.css','../../login/login.component.css']
})

export class HomeCarouselComponent implements OnInit, OnChanges {
  advertiseHomeScreen : Advertise[];
  showProject;
  showImage = 0;

  nextImage(){
    if(this.showImage === this.images.length-1){
      this.showImage = 0;
    } else{
      this.showImage ++;
    }

  }

  prevImage(){
    if(this.showImage === 0){
      this.showImage = 3;
    }else {
      this.showImage --;
    }
  }

  fillCircleOne(){
    if(this.showImage === 0){
      return 'black';
    }
  }
  fillCircleTwo(){
    if(this.showImage === 1){
      return 'black';
    }
  }
  fillCircleThree(){
    if(this.showImage === 2){
      return 'black';
    }
  }
  fillCircleFour(){
    if(this.showImage === 3){
      return 'black';
    }
  }

  setImageIndicators(imgNum: number){
    this.showImage = imgNum;
  }



  public images = IMAGES;

  constructor() {

  }

  ngOnInit() {
    this.advertiseHomeScreen = advertiseHomeScreen;
    setInterval(() => {
      this.showImage ++;
      if(this.showImage === this.images.length){
        this.showImage = 0;
      }
    }, 5000);

  }

  ngOnChanges() {


  }

}
