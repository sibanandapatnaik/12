import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { HomeComponent } from './home.component';
import { HomeRoutes } from './home.routes';
import { Footer } from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import {HomeCarouselComponent} from './home-page-carousel/home-carousel.component'
import {HomeNewsComponent} from './home-news-section/home-news.component'
import {HomeArticlesSectionComponent} from './home-articles-section/home-articles-section.component'
import {HomeContactsusSectionComponent} from './home-contactus-section/home-contactus-section.component'
import {HomeAboutusSectionComponent} from './home-aboutus-section/home-aboutus-section.component'
import { LoginComponent } from '../login/index';
import { NewsModule } from '../news/news.module';
import { ArticlesComponent } from '../articles/index';
import { AdvertiseSideComponent } from '../advertise/advertise-side-component/advertise-side-component';
import { ArticleDetailComponent } from '../article-detail/index';
import { NewsDetailComponent } from "app/news/news-detail";
import { NewsListComponent } from "app/news/news-list";
import { AboutusComponent } from "app/aboutus";
import { EditorComponent } from "app/editor/editor.component";
import { EventsListComponent } from 'app/events/events-list/events-list.component';
import {HomeNewsComponent1} from './home-news-section/home-news.service';

//import { LoginModule } from '../login/login.module';
@NgModule({
    imports: [CommonModule,  RouterModule.forChild(HomeRoutes)],
    declarations: [LoginComponent,ArticlesComponent,ArticleDetailComponent,NewsListComponent,NewsDetailComponent,AboutusComponent,EditorComponent,HomeComponent,Footer,HomeCarouselComponent, HomeNewsComponent,HomeArticlesSectionComponent,HomeContactsusSectionComponent,HomeAboutusSectionComponent,Topnav,AdvertiseSideComponent,EventsListComponent],
    exports: [HomeComponent],
    providers: [ HomeNewsComponent1 ],
})

export class HomeModule { }
