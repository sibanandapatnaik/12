import { Component, OnInit } from '@angular/core';
import { NewsSummary,newsList } from "app/models/news-summary";
import { Router } from "@angular/router";
import { Injectable } from '@angular/core';
import { Http , Response } from '@angular/http';
import { HomeNewsComponent1 }         from './home-news.service';
import 'rxjs/add/operator/toPromise';


@Component({
  selector: 'home-news-section',
  templateUrl: 'home-news.component.1.html',
   styleUrls: ['home-news.component.css','../../../assets/css/header.css','../../news/temp/css/2-col-portfolio.css'
  ]
})

@Injectable()
export class HomeNewsComponent implements OnInit  {

  newsList1:NewsSummary[] ;


  constructor(private router: Router,   private heroService: HomeNewsComponent1){
    
  }

  /*getHeroes(): void {
    this.heroService
        .getproducts()
        .then(newsList => this.newsList = newsList);
  }*/

  ngOnInit(): void {
 /*   console.log("xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
    this.heroService
    .getproducts().
    then(newsList1 => this.newsList1 = newsList1);
    alert(this.newsList1);*/

  }
  
  onNewsDetail(newsSelect : NewsSummary){
      console.log("method is called"+newsSelect.title)
      this.router.navigate(['/news-detail',newsSelect.uniqueId]);
}

}

