import { Component, OnInit } from '@angular/core';
import { NewsSummary,newsList } from "app/models/news-summary";
import { Router } from "@angular/router";

@Component({
  selector: 'home-news-section',
  templateUrl: 'home-news.component.1.html',
   styleUrls: ['home-news.component.css','../../../assets/css/header.css','../../news/temp/css/2-col-portfolio.css'
  ]
})

export class HomeNewsComponent  implements  OnInit {
   newsList:NewsSummary[] = new Array();
   newsSummary :NewsSummary;
  constructor(private router: Router){
    
  }
  ngOnInit(): void {
    //this.newsList = newsList.copyWithin(0,3);
   for (let i = 0; i < 6; i++) {
      this.newsSummary = newsList[i];
        this.newsList.push(this.newsSummary) ;
      
    }
    
     console.log("Length of header News: "+this.newsList.length)
  }
  
  onNewsDetail(newsSelect : NewsSummary){
      console.log("method is called"+newsSelect.title)
      this.router.navigate(['/news-detail',newsSelect.uniqueId]);
}

}

