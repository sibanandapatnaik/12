import { Routes } from '@angular/router';
import { NewsListComponent } from './news-list/news-list.component';
import { NewsDetailComponent } from "app/news/news-detail/news-detail.component";

//import { DashboardRoutes} from '../dashboard/index';
export const NewsRoutes: Routes = [
    {
      path: 'news', 
      component: NewsListComponent     
    },

     {
      path: 'news-detail/:id', 
      component: NewsDetailComponent     
    }
];
