import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NewsRoutes } from './news.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AdvertiseSideComponent } from '../advertise/advertise-side-component/advertise-side-component';
import { NewsListComponent } from "app/news/news-list/news-list.component";
import { NewsDetailComponent } from "app/news/news-detail/news-detail.component";




@NgModule({
    imports: [CommonModule,  RouterModule.forChild(NewsRoutes), BrowserModule,   HttpModule],
    declarations: [NewsListComponent,NewsDetailComponent, Footer , Topnav , AdvertiseSideComponent],
    exports: [NewsListComponent,NewsDetailComponent]
})

export class NewsModule {
}
