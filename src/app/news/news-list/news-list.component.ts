import { Component, OnInit } from '@angular/core';
import {NewsServices} from '../news.service'
import { NewsSummary } from "app/models/news-summary";
import { Observable }     from 'rxjs/Observable';
import { newsList }     from "app/models/news-summary";

import { advertiseCrowsel, Advertise, advertiseOne } from "app/models/advertise";
import { Router } from "@angular/router";
 //import * as data from 'articles.json';
const word = "{\\\"title\\\" : \\\"This is Articles 1";
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'news-list',
  templateUrl: 'news-list.component.1.html',
   styleUrls: ['../../../assets/css/home-page.css','../../../assets/css/responsive.css'
   ,'../../../assets/css/animate.css','../../../assets/css/style.css','../../login/login.component.css',
   '../../../assets/css/header.css','../newstheme/ie.css','../newstheme/style.css'],
   providers: [NewsServices]
})



export class NewsListComponent implements  OnInit {
   //articles: Observable<Article>;
   showImage = 0;
    newsList:NewsSummary[];
    private newsUniqueId: string;
    advertiseCrowsel : Advertise[];
    advertiseOne : Advertise;
  constructor(private router: Router,
    private newsService: NewsServices
  ){
  }
  ngOnInit() {
    console.log("Size "+newsList.length);
    this.newsList =newsList;
    this.advertiseCrowsel =advertiseCrowsel;
    this.advertiseOne = advertiseOne;
   // this.artilcesService.getArticles2();
    //this.articles = this.artilcesService.getArticles();
    //console.log("Size "+this.articles);
  }
onNewsDetail(newsSelect : NewsSummary){
  console.log("method is called"+newsSelect.title)
   this.router.navigate(['/news-detail',newsSelect.uniqueId]);
}
 }
