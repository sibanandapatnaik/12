import { Component, OnInit } from '@angular/core';
import {NewsServices} from '../news.service'
import { NewsSummary } from "app/models/news-summary";
import { Observable }     from 'rxjs/Observable';
import { detailMap, NewsDetail } from "app/models/news-detail";

import { advertiseCrowsel, Advertise ,advertiseOne } from "app/models/advertise";
import { ActivatedRoute, Params } from "@angular/router";
 //import * as data from 'articles.json';
const word = "{\\\"title\\\" : \\\"This is Articles 1";
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'news-detail',
  templateUrl: 'news-detail.component.html',
   styleUrls: ['../../../assets/css/home-page.css','../../../assets/css/responsive.css'
   ,'../../../assets/css/animate.css','../../../assets/css/style.css','../../login/login.component.css',
   '../../../assets/css/header.css','../../home/home-page-carousel/home-carousel.component.css','../../login/login.component.css'],
   providers: [NewsServices]
})

export class NewsDetailComponent implements  OnInit {
   //articles: Observable<Article>;
    detailMap: Map<string, NewsDetail>;
    newsDetail : NewsDetail;
    advertiseCrowsel : Advertise[];
    
    advertiseOne : Advertise;
    isSimple : boolean = true;
  constructor(
    private newsService: NewsServices,private activatedRoute: ActivatedRoute
  ){

  }
  ngOnInit() {
    let uniqueId;
    this.activatedRoute.params.subscribe((params: Params) => {
       uniqueId = params['id'];
        console.log("Receive i  this end "+uniqueId);
      });
    this.detailMap = detailMap;
    this.newsDetail = this.detailMap.get(uniqueId);
     console.log("Receive newsDetail title "+this.newsDetail.title);
    //newsDetailComplex;
    this.advertiseCrowsel = advertiseCrowsel;
     this.advertiseOne = advertiseOne;
     this.isSimple =  this.newsDetail.isSimple;
   // this.artilcesService.getArticles2();
    //this.articles = this.artilcesService.getArticles();
    //console.log("Size "+this.articles);
  }

  
 }
