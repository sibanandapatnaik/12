import { Injectable } from '@angular/core';
@Injectable()
export class GlobalConstant {
  BASE_URL : string = "http://52.66.104.137:8080";
  BASE_URL_1 : string = "http://localhost:8080";
  DATA_SERVICE_CONTEXT : string = "/magazine-data-service";
  DATA_SERVICE_CONTEXT_LOCAL : string = "";
  QUERY_PARAM_IS_DISPLAY_ON_MAIN_PAGE : string = "isDisplayOnMainPage"; 
  QUERY_PARAM_TOTAL_RECORDS : string = "totalRecords";
  QUERY_PARAM_START_INDEX : string = "startIndex";
  QUERY_PARAM_NAME: string = "name";
  API_CONTEXT_EVENT_SERACH : string = "/api/magazine/events/search/nonexpire";
  API_CONTEXT_UPLOAD_FILE : string = "/api/utility/files/upload/files";
 }
