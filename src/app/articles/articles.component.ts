import { Component, OnInit } from '@angular/core';
import {ArticlesServices} from './articles.service'
import { Article } from "app/models/article";
import { Observable }     from 'rxjs/Observable';
import { articlesList }     from "app/models/article";

import { advertiseCrowsel, Advertise, advertiseOne } from "app/models/advertise";
import { Router } from "@angular/router";
 //import * as data from 'articles.json';
const word = "{\\\"title\\\" : \\\"This is Articles 1";
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-articles',
  templateUrl: 'articles.component.html',
   styleUrls: ['../../assets/css/home-page.css','../../assets/css/responsive.css'
   ,'../../assets/css/animate.css','../../assets/css/style.css','../login/login.component.css',
   '../../assets/css/header.css','../shared/topnav/css/one-page-wonder.css','../shared/topnav/css/css/bootstrap-grid.css','../shared/topnav/css/css/bootstrap-grid.min.css'
   ,'../shared/topnav/css/css/bootstrap.css','../shared/topnav/css/css/bootstrap.min.css','../shared/topnav/css/css/bootstrap-reboot.css','../shared/topnav/css/css/bootstrap-reboot.min.css'

  ],
   providers: [ArticlesServices]
})



export class ArticlesComponent implements  OnInit {
   //articles: Observable<Article>;
    articless: Article[];
    private articleUniqueId: string;
    advertiseCrowsel : Advertise[];
    advertiseOne : Advertise;
  constructor(private router: Router,
    private artilcesService: ArticlesServices
  ){
  }
  ngOnInit() {
    console.log("Size "+articlesList.length);
    this.articless =articlesList;
    this.advertiseCrowsel =advertiseCrowsel;
    this.advertiseOne = advertiseOne;
   // this.artilcesService.getArticles2();
    //this.articles = this.artilcesService.getArticles();
    //console.log("Size "+this.articles);
  }
onArticleDetail(articleSelect : Article){
  console.log("method is called"+articleSelect.title)
   this.router.navigate(['/articles-detail',articleSelect.uniqueId]);
}
 }
