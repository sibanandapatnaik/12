/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './articles.component';
export * from './articles.routes';
