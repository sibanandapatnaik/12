import { Routes } from '@angular/router';
import { ArticlesComponent } from './index';
import { ArticleDetailComponent } from "app/article-detail";
//import { DashboardRoutes} from '../dashboard/index';
export const ArticlesRoutes: Routes = [
    {
      path: 'articles-page', 
      component: ArticlesComponent     
    },

     {
      path: 'articles-detail/:id', 
      component: ArticleDetailComponent     
    }
];
