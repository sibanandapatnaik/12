import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles.component';
import { ArticlesRoutes } from './articles.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AdvertiseSideComponent } from '../advertise/advertise-side-component/advertise-side-component';

   


@NgModule({
    imports: [CommonModule,  RouterModule.forChild(ArticlesRoutes), BrowserModule,   HttpModule],
    declarations: [ArticlesComponent, Footer , Topnav , AdvertiseSideComponent],
    exports: [ArticlesComponent]
})

export class ArticlesModule {
}
