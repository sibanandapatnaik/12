import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { advertiseCrowsel, Advertise, advertiseOne } from "app/models/advertise";
import { Router } from "@angular/router";
import { EventsServices } from 'app/events/events.service';
import { Events, eventList } from 'app/models/events';
import { GlobalConstant } from 'app/utils/global-constant';
import { CommonResponse } from 'app/models/commonresponse';
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'events-list',
  templateUrl: 'events-list.component.1.html',
  styleUrls: ['../../../assets/css/home-page.css', '../../../assets/css/responsive.css'
    , '../../../assets/css/animate.css', '../../../assets/css/style.css', '../../login/login.component.css',
    '../../../assets/css/header.css'],
  providers: [EventsServices,GlobalConstant]
})
export class EventsListComponent implements OnInit {
  eventList: Events[];
  advertiseCrowsel: Advertise[];
  advertiseOne: Advertise;
  urlBase : String = "http://localhost:8080/api/utility/files/download/";
  
  constructor(private router: Router,
    private eventsService: EventsServices,private globalConstant: GlobalConstant
  ) {
  }
  ngOnInit() {
   
    this.advertiseCrowsel = advertiseCrowsel;
    this.advertiseOne = advertiseOne;
    this.eventsService.getEventsMainPage(false).subscribe((posts: CommonResponse) => {
      console.log("Response Status "+ posts.status+" Message:: " + posts.message);
      this.eventList = Object.keys(posts.data).map(key => posts.data[key]);
    },error => {
    this.eventList = eventList;
  });
  }
}
