import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { EventsRoutes } from './events.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AdvertiseSideComponent } from '../advertise/advertise-side-component/advertise-side-component';
import { NewsListComponent } from "../news/news-list/news-list.component";
import { NewsDetailComponent } from "../news/news-detail/news-detail.component";
import { EventsListComponent } from '../events/events-list/events-list.component';




@NgModule({
    imports: [CommonModule,  RouterModule.forChild(EventsRoutes), BrowserModule,   HttpModule],
    declarations: [EventsListComponent, Footer , Topnav , AdvertiseSideComponent],
    exports: [EventsListComponent]
})

export class EventsModule {
}
