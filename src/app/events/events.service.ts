import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Article } from "../models/article";
import { GlobalConstant } from "../utils/global-constant";
import { CommonResponse } from '../models/commonresponse';

@Injectable()
export class EventsServices {
  private urlEventSearch: string;

  constructor(private http: Http, private globalConstant: GlobalConstant) {
    this.urlEventSearch = this.globalConstant.BASE_URL_1 + this.globalConstant.DATA_SERVICE_CONTEXT_LOCAL + this.globalConstant.API_CONTEXT_EVENT_SERACH;

  }

  public getEventsMainPage(mainPageDisplay : boolean): Observable<CommonResponse> {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    let mySearch: URLSearchParams = new URLSearchParams();
    mySearch.set(this.globalConstant.QUERY_PARAM_IS_DISPLAY_ON_MAIN_PAGE, mainPageDisplay+"");
    let options = new RequestOptions({ headers: headers }); // Create a request option
    options.params = mySearch;
    return this.http.get(this.urlEventSearch, options).map(response => response.json() as CommonResponse)
      .catch(err => {
        return Observable.throw(err); // observable needs to be returned or exception raised

      });
    }
  }