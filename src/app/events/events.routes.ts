import { Routes } from '@angular/router';
import { EventsListComponent } from '../events/events-list/events-list.component';

//import { DashboardRoutes} from '../dashboard/index';
export const EventsRoutes: Routes = [
    {
      path: 'events', 
      component: EventsListComponent     
    }
];
