import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ArticleDetailComponent } from './article-detail.component';
//import { ArticleDetailRoutes } from './article-detail.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { AdvertiseSideComponent } from '../advertise/advertise-side-component/advertise-side-component';

 


@NgModule({
    imports: [CommonModule, BrowserModule,   HttpModule],
    declarations: [ArticleDetailComponent, Footer , Topnav,AdvertiseSideComponent],
    exports: [ArticleDetailComponent]
})

export class ArticleDetailModule {
}
