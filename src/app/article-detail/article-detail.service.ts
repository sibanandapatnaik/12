import { Http,Response,RequestOptions ,Headers} from "@angular/http";
import { Observable }     from 'rxjs/Observable';
import { Article } from "app/models/article";
import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';

@Injectable()
export class ArticleDetailServices {

    constructor(private http: Http) {
        
    }

    public  getArticles(): Observable<Article> {
        console.log("getArticles  callled");
          return this.http.get("articles.json").map((response :Response) => {
            console.log("mock data" + response.json());
            return response.json() as Article }).catch(error => {
        // TODO: add real error handling
        console.log(error);
        return Observable.of<Article>();
      })

        }

          public  getArticles2() {
                let headers = new Headers();
headers.append('Content-Type', 'application/json');
//headers.append('Access-Control-Allow-Origin', '*');
//  headers.append('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
// headers.append('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Access-Control-Allow-Origin, Access-Control-Allow-Methods');
    let options       = new RequestOptions( headers); // Create a request option
  
        console.log("getArticles  callled 2");
          return this.http.get("./articles.json",options).map((response :Response) => {
            console.log("mock data" + response.json());
            return response.json() })
      

        }
}

   
