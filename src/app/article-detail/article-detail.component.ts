import { Component, OnInit,ViewChild, ElementRef, AfterViewInit } from '@angular/core';
import {ArticleDetailServices} from './article-detail.service'
import { Article } from "app/models/article";
import { Observable }     from 'rxjs/Observable';
import { detailMapArticle, ArticleDetail } from "app/models/article-detail";

import { advertiseCrowsel, Advertise ,advertiseOne } from "app/models/advertise";
import { ActivatedRoute, Params } from "@angular/router";
 //import * as data from 'articles.json';
const word = "{\\\"title\\\" : \\\"This is Articles 1";
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-article-detail',
  templateUrl: 'article-detail.component.html',
   styleUrls: ['../../assets/css/home-page.css','../../assets/css/responsive.css'
   ,'../../assets/css/animate.css','../../assets/css/style.css','../login/login.component.css',
   '../../assets/css/header.css'],
   providers: [ArticleDetailServices]
})

export class ArticleDetailComponent implements  OnInit,AfterViewInit  {
  
    //articles: Observable<Article>;
    detailMapArticle: Map<string, ArticleDetail>;
    advertiseCrowsel : Advertise[];
    articleDetail : ArticleDetail;
    advertiseOne : Advertise;
    isSimple : boolean = true;
  constructor(
    private artilcesService: ArticleDetailServices,private activatedRoute: ActivatedRoute
  ){

  }
  ngOnInit() {
    let uniqueId;
    this.activatedRoute.params.subscribe((params: Params) => {
       uniqueId = params['id'];
        console.log("Receive i  this end "+uniqueId);
      });
    this.detailMapArticle = detailMapArticle;
    this.articleDetail = this.detailMapArticle.get(uniqueId);
    this.advertiseCrowsel = advertiseCrowsel;
     this.advertiseOne = advertiseOne;
     this.isSimple = this.articleDetail.isSimple;
   // this.artilcesService.getArticles2();
    //this.articles = this.artilcesService.getArticles();
    //console.log("Size "+this.articles);
  }
//@ViewChild('iframeId') iframe: ElementRef

  ngAfterViewInit() {
   // console.debug("here vikash "+this.iframe.nativeElement.contentWindow.document.body.scrollHeight+ 'px');
   // console.debug("here vikash "+this.iframe.nativeElement.scrollHeight+ 'px');
   // this.iframe.nativeElement.style.height = 0;//this.iframe.nativeElement.contentWindow.document.body.scrollHeight
   // this.iframe.nativeElement.style.height = 2850+this.iframe.nativeElement.contentWindow.document.body.scrollHeight+ 'px';
  // this.iframe.nativeElement.style.width="100%";
    //this.iframe.nativeElement.sc.sc
  }

  
 }
