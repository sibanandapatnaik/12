import { Http, Headers, RequestOptions, URLSearchParams } from '@angular/http';
import { Observable } from 'rxjs/Observable';

import 'rxjs/add/operator/map'
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/of';
import { Article } from "app/models/article";
import { GlobalConstant } from "app/utils/global-constant";
import { CommonResponse } from 'app/models/commonresponse';

@Injectable()
export class FileUploadService {
  private urlFileUpload: string;

  constructor(private http: Http, private globalConstant: GlobalConstant) {
    this.urlFileUpload = this.globalConstant.BASE_URL_1 + this.globalConstant.DATA_SERVICE_CONTEXT_LOCAL + this.globalConstant.API_CONTEXT_UPLOAD_FILE;

  }

  public uploadFileToServer(file): Observable<CommonResponse> {
    let headers = new Headers();
    headers.set('Content-Type', "multipart/form-data;boundary=----");
   // headers.set('Accept', 'application/json');
   headers.append('Accept', 'application/json');
   // let mySearch: URLSearchParams = new URLSearchParams();
    let formData:FormData = new FormData();
    formData.append("upload-document", file);
    let options = new RequestOptions({ headers: headers }); // Create a request option
    //options.params = mySearch;
     options.body = formData;
    return  this.http.post(this.urlFileUpload,options).map(response => response.json() as CommonResponse)
      .catch(err => {
        return Observable.throw(err); // observable needs to be returned or exception raised

      });
    }
  }