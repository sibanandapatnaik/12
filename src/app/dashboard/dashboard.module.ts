import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard.component';
import {TopbarComponent} from './shared/topbar-dashboard/topbar.component'
import { FileUploadModule } from 'primeng/primeng';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ArticlesComponent } from "app/dashboard/articles";
import { LeftsidebarComponent } from "app/dashboard/shared/leftsidebar-dashboard/leftsidebar.component";

import { NewsAdminAddComponent } from "app/dashboard/news-admin/news-admin-add/news.admin.add.component";
//import { EditorModule } from "primeng/components/editor/editor";
//import { CodeHighlighterModule } from "primeng/components/codehighlighter/codehighlighter";
//import { TabViewModule } from "primeng/components/tabview/tabview";
@NgModule({
    imports: [BrowserModule, FormsModule,CommonModule, RouterModule,FileUploadModule ],
    declarations: [DashboardComponent,TopbarComponent,LeftsidebarComponent,ArticlesComponent,NewsAdminAddComponent],
    exports: [DashboardComponent,ArticlesComponent,NewsAdminAddComponent]
})

export class DashboardModule { }
