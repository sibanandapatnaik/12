import { Component, OnDestroy, AfterViewInit, EventEmitter, Input, Output, ElementRef, ViewChild } from '@angular/core';
import { RequestOptions } from "@angular/http";
import { FileUploadService } from 'app/dashboard/file-upload/file-upload.service';
import { GlobalConstant } from 'app/utils/global-constant';
import { CommonResponse } from 'app/models/commonresponse';

/**
*  This class represents the lazy loaded LoginComponent.
*/
const URL = 'http://localhost:8000/api/upload';
@Component({
  selector: 'app-admin-news-add',
  templateUrl: 'news.admin.add.component.html',
   styleUrls: ['../../material-dashboard.css'],
   providers : [FileUploadService,GlobalConstant]
})
 
 

export class NewsAdminAddComponent implements AfterViewInit, OnDestroy {
  @Input() elementId: String = "elemy";
  @ViewChild('fileInput') inputEl: ElementRef;
  selectedOption:string = 'option1';  //to set a default selected value provide the value here
  constructor(
    private fileUploadService: FileUploadService,private globalConstant: GlobalConstant
  ) {
  }
  @Output() onEditorKeyup = new EventEmitter<any>();
 msgs: string[];
  editor;
    text: string;

  ngAfterViewInit() {
    tinymce.init({
      selector: '#' + this.elementId,
      plugins: ['link', 'paste', 'table'],
      skin_url: 'assets/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyup.emit(content);
        });
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
    
    uploadedFiles: any[] = [];

    onUpload(event) {
      console.log("filre is uploaded :::>");
        for(let file of event.files) {
           console.log("fil32 is uploaded :::>"+file.size);
           this.fileUploadService.uploadFileToServer(file).subscribe((posts: CommonResponse) => {
            console.log("Response Status "+ posts.status+" Message:: " + posts.message);
           });
            //this.uploadedFiles.push(file);
        }
    
        this.msgs = [];
        //this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
    }
       myUploader(event) {
      console.log("file 11 is uploaded :::>");
        for(let file of event.files) {
           console.log("filre 22 is uploaded :::>");
            this.uploadedFiles.push(file);
        }
    
        this.msgs = [];
        //this.msgs.push({severity: 'info', summary: 'File Uploaded', detail: ''});
    }
 
/*
  fileChange(event) {
    let fileList: FileList = event.target.files;
    if(fileList.length > 0) {
        let file: File = fileList[0];
        let formData:FormData = new FormData();
        formData.append('uploadFile', file, file.name);
        let headers = new Headers();
        /* No need to include Content-Type in Angular 4 
        headers.append('Content-Type', 'multipart/form-data');
        headers.append('Accept', 'application/json');
        let options = new RequestOptions({ headers: headers });
        this.http.post(`${this.apiEndPoint}`, formData, options)
            .map(res => res.json())
            .catch(error => Observable.throw(error))
            .subscribe(
                data => console.log('success'),
                error => console.log(error)
            )
}
}*/
}
