import { Routes, Route } from '@angular/router';
import { DashboardComponent } from './index';
import { ArticlesComponent } from "app/dashboard/articles";
import { NewsAdminAddComponent } from "app/dashboard/news-admin/news-admin-add/news.admin.add.component";
export const DashboardRoutes: Route[] = [
    {
      path: 'dashboard',
      component: DashboardComponent
    },{
      path: 'dashboard/admin-article-add',
      component: ArticlesComponent
    },{
      path: 'dashboard/admin-news-add',
      component: NewsAdminAddComponent
    }
];
