import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-dashboard-cmp',
  templateUrl: 'dashboard.component.html',
   styleUrls: ['material-dashboard.css']
})

export class DashboardComponent { }
