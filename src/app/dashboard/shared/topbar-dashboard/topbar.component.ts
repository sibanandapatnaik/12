import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'dashboard-topbar-cmp',
  templateUrl: 'topbar.component.1.html',
   styleUrls: ['../../material-dashboard.css']
})

export class TopbarComponent { }
