import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'dashboard-leftsidebar-cmp',
  templateUrl: 'leftsidebar.component.1.html',
   styleUrls: ['../../material-dashboard.css']
})

export class LeftsidebarComponent { }
