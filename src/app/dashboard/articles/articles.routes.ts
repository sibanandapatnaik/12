import { Routes, Route } from '@angular/router';
import { ArticlesComponent } from './index';
export const ArticlesRoutes: Route[] = [
    {
      path: 'articles',
      component: ArticlesComponent
    }
];
