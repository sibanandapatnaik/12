import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ArticlesComponent } from './articles.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [ArticlesComponent],
    exports: [ArticlesComponent]
})

export class ArticlesModule { }
