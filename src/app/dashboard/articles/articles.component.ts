import { Component,OnDestroy,AfterViewInit,EventEmitter,Input,Output } from '@angular/core';
/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-articles',
  templateUrl: 'articles.component.html',
   styleUrls: ['articles.component.css']
})

export class ArticlesComponent implements AfterViewInit, OnDestroy {
  @Input() elementId: String = "test-my";
  @Output() onEditorKeyup = new EventEmitter<any>();

  editor;

  ngAfterViewInit() {
    tinymce.init({
      selector: '#' + this.elementId,
      plugins: ['link', 'paste', 'table'],
      skin_url: 'assets/skins/lightgray',
      setup: editor => {
        this.editor = editor;
        editor.on('keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyup.emit(content);
        });
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }
}
