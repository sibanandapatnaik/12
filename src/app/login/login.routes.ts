import { Routes } from '@angular/router';
import { LoginComponent } from './index';
//import { DashboardRoutes} from '../dashboard/index';
export const LoginRoutes: Routes = [
    {
      path: 'login', 
      component: LoginComponent     
    }
];
