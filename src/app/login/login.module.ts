import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { LoginComponent } from './login.component';
import { LoginRoutes } from './login.routes';
import { Footer} from '../shared/footer/index';
import { Topnav} from '../shared/topnav/index';
////import { DashboardModule } from '../dashboard/dashboard.module';

@NgModule({
    imports: [CommonModule,  RouterModule.forChild(LoginRoutes)],
    declarations: [LoginComponent, Footer , Topnav],
    exports: [LoginComponent, Footer, Topnav]
})

export class LoginModule { }
