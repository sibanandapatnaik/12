import { Component,OnInit } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-editor-cmp',
  templateUrl: 'editor.component.html',
   styleUrls: ['../login/login.component.css','../../assets/css/header.css','../../assets/css/home-page.css','../../assets/css/responsive.css']
})

export class EditorComponent implements  OnInit{
    editorText : string;
    editorTitle: string;
     ngOnInit() {
     this.editorText = "<p>&nbsp;</p> <p><span style=\"font-weight: 400;\">Poultry management is differentiated according to the existing climatic conditions. This is why these days the best poultry farms adhere to environmentally controlled poultry houses. Anyway, the climate in the rainy season is humid, which means surplus of moisture in the air. This very moisture in the air brings with it a series of problems at the poultry farm. </span></p> <p><span style=\"font-weight: 400;\">First of all, there should be a complete control on the water quality that is fed via drinkers to the birds. Apart from this, the farm management should get the quality check done on the feed and the additives to be fed to the birds, if it has been preserved for a long period. The feed when it is exposed to the moisture develops fungal and other strains that contaminate the feed and so it is no longer fit to be fed to the birds. If such a contaminated fed is fed to the birds that the expected infection in the flock can add to the medical expenses and in the worst case mortality of the birds.</span></p> <p><span style=\"font-weight: 400;\">Apart from this, the humid environment inside the poultry house is a precursor to scores of diseases that may cause huge complications in so many ways. This is why the latest trend in rearing of poultry birds has come up with the latest option of environmentally controlled poultry houses. In such houses the adequate levels of feel good environment level for the birds is duly maintained. Though it is certainly costly to have such houses but one this is confirmed that the farmer has nothing to worry about the outside climatic fluctuations.</span></p> <p><span style=\"font-weight: 400;\">The overall biosecurity measures should always be in place throughout the year and very vigilant especially during the rainy season. The litter management should be exceptional because when the litter mixes with the moisture, it liberates ammonia which is fatal to the respiration of birds, again opening up probabilities for various infections. Besides this, the contaminated litter when comes in the contact of the bird&rsquo;s legs, there are again chances of infection. So a poultry farmer should be extra cautious to expertly handle the management of the poultry birds in the rainy season.</span></p> <p><span style=\"font-weight: 400;\">-Editor</span></p>";
     this.editorTitle = "Poultry Management in Rainy season";
   }

}