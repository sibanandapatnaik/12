import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { routes } from './app.routes';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { HomeModule } from './home/home.module';
import { DashboardModule } from './dashboard/dashboard.module';
import { NewsModule } from "./news/news.module";

//import { LoginModule } from './login/login.module';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
  //  NewsModule,
    RouterModule.forRoot(routes,
      { enableTracing: true }),
    HomeModule,
    DashboardModule
  //  LoginModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
 