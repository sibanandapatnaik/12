import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';

import { LoginComponent } from './login/index';

import { LoginRoutes } from './login/login.routes';
import { DashboardRoutes } from './dashboard/dashboard.routes';
import { ArticlesRoutes } from './articles/articles.routes';
import { NewsRoutes } from "./news/news.routes";
import { HomeComponent } from "./home";
import { HomeRoutes } from "./home/home.routes";
import { AboutusRoutes } from "./aboutus";
import { EventsRoutes } from './events';

//import { ArticleDetailRoutes } from './article-detail/article-detail.routes';

export const routes: Routes = [
   ...LoginRoutes,
  ...DashboardRoutes,
  ...HomeRoutes,
  ...ArticlesRoutes,
  ...NewsRoutes,
  ...AboutusRoutes,
  ...EventsRoutes,
   { path: '**', component: HomeComponent }
 
]
