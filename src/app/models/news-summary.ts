export class NewsSummary{
    uniqueId : string ;
    title : String;
    thumbnail : String;
    shortDesc : String;
    createdDate : string;
     place : string;

    
}
export const newsList: NewsSummary[] = [
  { 
    uniqueId: "1",
    "title" : "31 ST  Edition of Space 2017, a grand success!", 
    shortDesc: "France who spent over five hours at the Expo to get the full taste of the show,",
    thumbnail  : "assets/news/2017/Oct/2.JPG",
    createdDate : "June 13 & 14, 2017",
    place : "The France"
 },
 { 
    uniqueId: "2",
    title: "PFI conducts AGM and Technical Seminar at Gurugram",
    shortDesc: "The Poultry Federation of India (PFI) conducted its Annual General Meeting (AGM) and Technical Seminar on 23rd August, 2017 at Hotel The Leela Ambience at Gurugram, Haryana",
    thumbnail  : "assets/news/2017/Oct/3.JPG",
    createdDate : "September, 2017",
    place : "Gurugram",
 },
 { 
    uniqueId: "3",
    title: "IVPI conducts a successful Annual Scientific Programme at Bengaluru",
    shortDesc: "The Institution of Veterinarians of Poultry Industry (IVPI) conducted an informative and successful Annual Scientific programme",
    thumbnail  : "assets/news/2017/Oct/5.JPG",
    createdDate : "September 2nd, 2017",
     place : "Bengaluru, KA"
 },
 {  uniqueId: "4",
 title: "Successful RESCOM SUMMIT 2017 held at Jaipur",
 shortDesc: "RESCOM SUMMIT 2017, an initiative of Huvepharma SEA (Pune) Pvt. Ltd., is the second in the series of technical conferences mainly focused on RESpiratory COMplex in the poultry segment",
    thumbnail  : "assets/news/2017/Oct/6.JPG",
    createdDate : "August 17,19 , 2017",
    place : "Jaipur"
 },
 {  uniqueId: "5",
 title: "INFAH conducts  its 6th AGM at Mumbai ",
 shortDesc: "The 6th Annual General Body meeting of INFAH was held on 11th August 2017 at Hotel The Leela Mumbai.",
 thumbnail  : "assets/news/2017/Oct/7.JPG",
    createdDate : "July 6-8, 2017",
    place : "Mumbai"
 },{
  uniqueId: "6",
  title: "Dr. Sushiel Agrawal honoured with INFAH Award 2017",
  shortDesc: "The Chairman of Indian Herbs, Saharanpur (India), Dr. Sushiel Agrawal, is honoured with the he prestigious INFAH Award 2017 by Indian Federation of Animal Health Companies, popularly known as INFAH",  
  thumbnail  : "assets/news/2017/Oct/8.JPG",
     createdDate : "August, 2017",
     place : "Mumbai"
 }

];