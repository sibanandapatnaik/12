export class Events {
    uniqueId : string;
    displayMainPage: Boolean = false;
    thumbnailId: string;
    dateOfEvent: String;
    venue: String;
    detailUrl: String = "";
    title: string;
    shortDesc: string = "";
    sib:string="";

    
}
export const eventList : Events[] = [
    {
        uniqueId : "1",
        displayMainPage : false,
        title : "SPACE 2017",
        thumbnailId : "assets/img/events/events1.png",
        dateOfEvent : "September 12, 2017 to September 15, 2017",
        venue : "Parc Expo of Rennes Airport Rennes - Bretagne – France",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar1"

    },
    {
        uniqueId : "2",
        displayMainPage : false,
        title : "CLFMA of INDIA",
        thumbnailId : "assets/img/events/events2.png",
        dateOfEvent : "15th & 16th September 2017",
        venue : "JW Marriott Mumbai Sahar, Near International Airport at Mumbai, Maharashtra, INDIA",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar2"


    }, {
        uniqueId : "3",
        displayMainPage : false,
        title : "Poultry Fest 2017 - 6th Edition",
        thumbnailId : "assets/img/events/events3.png",
        dateOfEvent : "06-07-08 October",
        venue : "Lucknow (India)",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar3"

    }, {
        uniqueId : "4",
        displayMainPage : false,
        title : "NEPAL POULTRY & LIVESTOCK EXPO 2017",
        thumbnailId : "assets/img/events/events4.png",
        dateOfEvent : "November 3 - 4 - 7",
        venue : "Bharatpur, Chitwan, Nepal",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar4"

    },
    {
        uniqueId : "5",
        displayMainPage : false,
        title : "Poultry India 2017",
        thumbnailId : "assets/img/events/events5.png",
        dateOfEvent : "November 22, 2017 to November 24, 2017",
        venue : "Hitex Exhibition Complex, Hyderabad, India D.No. 11-7-201, 1st Floor, Above Bank Of India ATM,, HUDA Complex, Saroornagar, Hyderabad, Telangana 500035, India - Hyderabad - India",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar5"

    },
   
    {
        uniqueId : "6",
        displayMainPage : false,
        title : "IPPE - International Production & Processing Expo 2018",
        thumbnailId : "assets/img/events/events6.png",
        dateOfEvent : "January 30, 2018 to February 1, 2018",
        venue : "Georgia World Congress Center 285 Andrew Young International Blvd NW - Atlanta - Georgia - United States",
        detailUrl : "",
        shortDesc : "",
        sib:"menu-cust-bar6"

    }
    ,
    
     {
         uniqueId : "7",
         displayMainPage : false,
         title : "VIV Europe 2018 Utrecht",
         thumbnailId : "assets/img/events/events7.png",
         dateOfEvent : "20-22 June 2018",
         venue : "Jaarbeurs , UTRECHT , Netherland",
         detailUrl : "",
         shortDesc : "",
         sib:"menu-cust-bar1"
 
     }
     ,
      {
          uniqueId : "8",
          displayMainPage : false,
          title : "ABC Challenge Asia",
          thumbnailId : "assets/img/events/events8.png",
          dateOfEvent : "October 17, 2017",
          venue : "JW Marriott Hotel Jakarta Jalan DR Ide AnakAgungGdeAgungKav E.1.2 No. 1 & 2,, Kawasan Mega Kuningan, RT.5/RW.2, East Kuningan, Jakarta, 12950, Indonesia - Jakarta – Indonesia",
          detailUrl : "",
          shortDesc : "",
          sib:"menu-cust-bar2"
  
      },
      
      {
          uniqueId : "9",
          displayMainPage : false,
          title : "XXIII European Symposium on the Quality of Poultry Meat and XVII European Symposium on the Quality of Eggs and Egg Products",
          thumbnailId : "assets/img/events/events9.png",
          dateOfEvent : "03 September 2017 - 05 September 2017",
          venue : "John McIntyre Conference Centre City: Edinburgh, United Kingdom",
          detailUrl : "",
          shortDesc : "",
          sib:"menu-cust-bar3"
  
      },
      
      {
          uniqueId : "10",
          displayMainPage : false,
          title : "Poultry Africa 2017",
          thumbnailId : "assets/img/events/events10.png",
          dateOfEvent : "04 October 2017 - 05 October 2017",
          venue : "Kigali Convention Centre	| City: Kigali, Rwanda",
          detailUrl : "",
          shortDesc : "",
          sib:"menu-cust-bar4"
  
      },
      
      {
          uniqueId : "11",
          displayMainPage : false,
          title : "VIV MEA 2018 - ABU DHABI",
          thumbnailId : "assets/img/events/events11.png",
          dateOfEvent : "FEBRUARY 5-7, 2018",
          venue : "Abu Dhabi National Exhibition Center Abu Dhabi , United Arab Emirates ",
          detailUrl : "",
          shortDesc : "",
          sib:"menu-cust-bar5"
  
      }
];