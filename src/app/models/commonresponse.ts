/**
 * New typescript file
 */
export  class CommonResponse {
 public status: number;
 public message: string;
 public data: Object ;
}
