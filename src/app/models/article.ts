export class Article{
    uniqueId : string ;
    title : String;
    thumbnail : String;
    shortDesc : String;
    createdDate : string;
    authorName : string;

    
}
export const articlesList: Article[] = [
  { 
    uniqueId : "1",
    title : "Historical Perspective of Methionine Sources", 
    shortDesc: "Metabolisable Energy” (ME) is a concept used to characterize the nutritional value of bird feedstuffs. It is an estimate of the energy available to a bird from digestion of a feed material and available to the bird for metabolic processes.",
    thumbnail  : "assets/img/events/Poultry1.jpg",
    createdDate : "August, 2017",
    authorName : "Nirmala Muwel1, Chandrakanta Rawat2, Shilpa Choudhary1, Mokshata Gupta",
 },
 { 
    uniqueId : "2",
    title : "Factors affecting metabolisable energy value of poultry feed- At a glance", 
    shortDesc: "IMetabolisable Energy” (ME) is a concept used to characterize the nutritional value of bird feedstuffs. It is an estimate of the energy available to a bird from digestion of a feed material and available to the bird for metabolic processes.",
    thumbnail  : "assets/img/events/poultry2.jpg",
    createdDate : "August 6-8, 2017",
    authorName : "Dr.K.Sudheer1, Dr.V.Govind2 and Dr.R.Jayanthi2"
 },
 { 
  uniqueId : "3",
  title : "EFFECT OF NUTRIKEM™ XLP PLUS SUPPLEMENT ON PERFORMANCE AND ECONOMIC INDICES IN LAYING HENS", 
  shortDesc: "In several animal production systems, feed is the greatest single cost and productivity can rely on the relative cost and nutritive estimation of the feed out there",
  thumbnail  : "assets/img/events/poultry3.jpg",
  createdDate : "August, 2017",
  authorName : "Bharat L Sadarao, Chandrasekar Selvaraj and Saravanan Sankaran",
 },
 { 
  uniqueId : "4",
  title : "LIGHT MANAGEMENT IN POULTRY FARM", 
  shortDesc: "Light management is an integral part of poultry farming. Poultryare very much photosensitive which affect their production and reproduction traits",
  thumbnail  : "assets/img/events/poultry4.jpg",
  createdDate : "August, 2017",
  authorName : "Vivek Kumar Nayak1, Sudipta Kumar Panda",
 },
 { 
  uniqueId : "5",
  title : "Role of vitamin E in poultry nutrition and reproduction", 
  shortDesc: "Vitamins are vital organic compounds that can be obtained in high quantities from vegetable oils. Vitamin tablets are also commercially available at a reasonable price. To date, thirteen vitamins have been discovered, named alphabetically vitamin A to vitamin K. The fat-soluble vitamin E was discovered in the early 1920s by Evans and Bishop. Vitamin E is a generic term for a group of tocopherols and tocotrienols that have some amount of vitamin activity",
  thumbnail  : "assets/img/events/Poultry1.jpg",
  createdDate : "August, 2017",
  authorName : "Mir Mudasir1, Manzoor Ahmad Bhat2*, Nawab Nashiruddulla1, Moien Javaid3, Azhar Shuaib Bato4, Faizan Javai5",
 },
 { 
  uniqueId : "6",
  title : "POULTRY - BROILER   PRODUCTION AND ITS MANAGEMENT", 
  shortDesc: "Broiler is a bird of about 8 weeks of age of either sex (straight-run chicks) with an average body weight of 1.5 to 2.0 kg with a flexible breast bone cartilage, pliable and tender meat. Broilers can be housed on deep-litter, slatted or wire floor or cages.  However, cage, slat and wire floor rearing of broilers are not as popular as litter floor rearing, due to problems like breast blisters, leg weakness and higher initial investment. ",
  thumbnail  : "assets/img/events/poultry2.jpg",
  createdDate : "August, 2017",
  authorName : "VS. Ilavarasan,  * R. Ilavarasan, **T.A. Vijayalingam and N.V. Rajesh",
 },
 { 
  uniqueId : "7",
  title : "Selection of poultry bedding and its management", 
  shortDesc: "Good floor management starts with selecting the bedding material. The ideal material consists of dry friable particles that will stimulate a bird’s natural behaviour to dust-bathe. Absorbency and “friability” are the most important factors in selecting appropriate bedding materials. Any material used under birds has to be like a sponge in that it will soak up moisture and give up the moisture to the ventilation system so that it stays dry.",
  thumbnail  : "assets/img/events/poultry3.jpg",
  createdDate : "August, 2017",
  authorName : "Vikram jakhar",
 },
 { 
  uniqueId : "8",
  title : "TRANSGENESIS IN CHICKEN", 
  shortDesc: "Transgenesis is the process of introducing an exogenous gene (transgene) into the genome of the animal. A transgenic chicken carries the recombinant gene that is intentionally introduced artificially in contrast to spontaneous mutation. The foreign gene integrated into the genome of the transgenic animal is called 'transgene' and the protein coded by the transgene is called as 'Transgenic product'. Transgenesis includes the stable integration of the transgene within the genome of host and its transmission to progeny through normal breeding programmes Methodology",
  thumbnail  : "assets/img/events/poultry4.jpg",
  createdDate : "August, 2017",
  authorName : "Rajesh Kumar Sahu1*, Amiya Ranjan Sahu2 and Vandana Yadav2",
 }
 ];